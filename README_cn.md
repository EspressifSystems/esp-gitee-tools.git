# 介绍

本仓库包含给 gitee 其他仓库提供的扩展工具，包括镜像仓库使用、submodule 更新、开发工具安装等，可加速环境的搭建。

## 镜像仓库使用（推荐使用）

用于将 github 仓库的 url 替换成镜像仓库的 url，详细使用请参看 [jihu-mirror 使用](./docs/README-jihu-mirror.md)

## submodule-update（不推荐使用）

用于 ESP-IDF 等仓库内 submodules 的更新，详细使用请参看 [submodule-update 使用](./docs/README-submodule-update.md)。

## install

用于 ESP-IDF 开发工具的安装，详细使用请参看 [install 使用](./docs/README-install.md)。